$(document).ready(function () {
	initScroll();
	initInputs();
	$('.what > .container').parallax();
	initSlider();
	initSliderPoint();
	initSubOpen();
	$('select').select2({
		templateSelection: function (data) {
			if (data.class === 'empty') { // adjust for custom placeholder values
				alert('ololo');
			}

			return data.text;
		}
	});

	$(".nano").nanoScroller();
	initFAQ();
	initRoadmap();
	initCredSlider();
	initNews();
	initPoap();
	initRoadmapIndex();
	initDashHistory();
	initScannerAdd();
	initNotif();

	$('ul.tabs').each(function () {
		$(this).tabs();
	});

	$('.cases-slider-index').slick({
		infinite: false,
		slidesToShow: 2,
		slidesToScroll: 2,
		variableWidth: true,
		dots: false,
		arrows: false,
		swipeToSlide: true,
		responsive: [{
			breakpoint: 750,
			settings: {
				variableWidth: false
			}
		}, {
			breakpoint: 680,
			settings: {
				variableWidth: false,
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
});

function initNotif() {
	$('.cover-notif').each(function () {
		var hold = $(this);
		var elem = hold.find('.hold-notif');
		var eltext = elem.find('.wrap');
		var links = hold.find('.close, .toggle');
		var linkall = hold.find('.toggle-all');
		var options = {
			queue: true,
			duration: 200,
			easing: 'linear'
		};

		elem.each(function () {
			var thisel = $(this);
			var link = thisel.find('.close, .toggle-this');
			var text = thisel.find('.wrap');

			link.click(function () {
				thisel.toggleClass('open');
				text.slideToggle(options);
			});
		});

		links.click(function () {
			if (elem.hasClass('open')) {
				hold.removeClass('hidden-all');
			} else {
				hold.addClass('hidden-all');
			}
		});

		linkall.click(function () {
			hold.removeClass('hidden-all');
			elem.addClass('open');
			eltext.slideDown(options);
		});
	});
}

function initScannerAdd() {
	$('.scanner > .container > .add-new').each(function () {
		var hold = $(this);
		var link = hold.find('.toggle > .btn');
		var text = hold.find('.in');
		var options = {
			queue: true,
			duration: 200,
			easing: 'linear'
		};

		link.click(function () {
			hold.toggleClass('open');
			text.slideToggle(options);
		});
	});
}

function initDashHistory() {
	$('.dashboard-new').each(function () {
		var hold = $(this);
		var link = hold.find('.link-open-history, .link-close-history, .dash-fader');
		link.click(function () {
			hold.toggleClass('open-history');
		});
	});
}

function initFAQ() {
	jQuery('.section.faq > .part > .container > .l').each(function () {
		var hold = jQuery(this);
		var box = hold.find('h2');

		var _scroll = function () {
			var top = (hold.offset().top - 120);

			if ($(window).scrollTop() > top) {
				if ($(window).scrollTop() > top + hold.outerHeight() - box.outerHeight(true)) {
					box.css({
						position: 'relative',
						left: 0,
						top: hold.outerHeight() - box.outerHeight(true) - 120
					});
				} else {
					box.css({
						position: 'fixed',
						left: hold.offset().left,
						top: 120
					});
				}
			} else {
				box.css({
					position: 'static',
					left: 0,
					top: 0
				});
			}
		};

		jQuery(window).bind('scroll resize', _scroll);

		_scroll();
	});
}

function initSlider() {
	var slideshow = $('.cases-slider');

	slideshow.slick({
		infinite: true,
		arrows: false,
		dots: false,
		adaptiveHeight: true
	});

	var linkNext = slideshow.find('.next');

	linkNext.click(function () {
		slideshow.slick('slickGoTo', parseInt(slideshow.slick('slickCurrentSlide')) + 1);

	});

}

function initSliderPoint() {
	var slideshow = $('.point .image.gall');

	slideshow.slick({
		fade: true,
		infinite: true,
		arrows: false,
		dots: true,
		autoplay: true,
		autoplaySpeed: 2000
	});

	var linkNext = slideshow.find('.slick-slide');

	linkNext.click(function () {
		slideshow.slick('slickGoTo', parseInt(slideshow.slick('slickCurrentSlide')) + 1);

	});

}

function initInputs() {
	$('.input input, .input textarea').each(function () {
		var _el = $(this);
		var _val = _el.val();

		if (_val == '') {
			$(this).parent().addClass('placeholder');
		}

		_el.bind('focus', function () {
			if (this.value == _val) {
				$(this).parent().removeClass('placeholder');
			}
			$(this).parent().addClass('input-focus');
		}).bind('blur', function () {
			if (this.value == '') {
				$(this).parent().addClass('placeholder');
			} else {
				$(this).parent().removeClass('placeholder');
			}
			$(this).parent().removeClass('input-focus');
		});
	});
}

function initSubOpen() {
	$('.has-sub').each(function () {
		var hold = $(this);
		var link = hold.find('.toggle');
		var sub = hold.find('.sub');

		link.hover(function () {
			hold.addClass('hover');
		}, function () {
			hold.removeClass('hover');
		});

		sub.hover(function () {
			hold.addClass('hover');
		}, function () {
			hold.removeClass('hover');
		});

		link.click(function () {
			hold.toggleClass('open');
		});

		$('body').click(function () {
			if (!hold.hasClass('hover')) {
				hold.removeClass('open');
			}
		});

	});
}

function initRoadmap() {
	$('.road-nav').each(function () {
		var hold = $(this);
		var link = hold.find('.links > a');

		var _scroll = function () {
			link.removeClass('active');
			link.each(function () {
				if ($($(this).attr('href')).offset().top < $(window).scrollTop() + 200) {
					link.removeClass('active');
					$(this).addClass('active');
				}
			});
		};

		_scroll();

		link.click(function () {
			$('html, body').animate({
				scrollTop: $($(this).attr('href')).offset().top - 200
			}, {
				queue: false,
				duration: 1500
			});
			return false;
		});

		$(window).bind('scroll', _scroll);
	});
}

function initCredSlider() {
	// slider
	$slick_slider = $('.credentials .list');
	settings_slider = {
		dots: false,
		arrows: true,
		slidesToShow: 2,
		slidesToScroll: 2,
		responsive: [{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}

		]
	};

	settings_team = {
		dots: false,
		centerMode: true,
		arrows: false,
		centerPadding: '5%',
		slidesToShow: 1,
		slidesToScroll: 2,
		responsive: [{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}

		]
	};

	settings_credentials = {
		dots: false,
		centerMode: true,
		prevArrow: false,
		arrows: true,
		centerPadding: '5%',
		slidesToShow: 2,
		slidesToScroll: 2,
		responsive: [{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}

		]
	};

	settings_partners = {
		dots: false,
		centerMode: false,
		prevArrow: false,
		arrows: true,
		centerPadding: '5%',
		slidesToShow: 2,
		slidesToScroll: 2,
		responsive: [{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}

		]
	}

	slick_on_mobile($slick_slider, settings_slider);
	slick_on_mobile($('.coreteam-block'), settings_team);
	slick_on_mobile($('.credentials-list'), settings_credentials);
	slick_on_mobile($('.partnersnew__list'), settings_partners);

	// slick on mobile
	function slick_on_mobile(slider, settings) {
		$(window).on('load resize', function () {
			if ($(window).width() > 750) {
				if (slider.hasClass('slick-initialized')) {
					slider.slick('unslick');
				}
				return;
			}

			if (!slider.hasClass('slick-initialized')) {
				slider.slick(settings);
			}
		});
	}
}

$(window).on('load resize', function () {
	if ($(window).width() < 750) {
		
	}
});

function initNews() {
	$('ul.news-list').each(function () {
		var hold = $(this);
		var el = hold.find('> li');
		var options = {
			queue: true,
			duration: 200,
			easing: 'linear'
		};

		el.each(function () {
			var thisEl = $(this);
			var link = thisEl.find('h3 > span');
			var text = thisEl.find('.in');

			link.click(function () {
				if (thisEl.hasClass('open')) {
					text.slideUp(options);
					thisEl.removeClass('open');
				} else {
					text.slideDown(options);
					thisEl.addClass('open');
				}
			});
		});
	});
}

function initPoap() {
	$('.poap').each(function () {
		var hold = $(this).find('.bg');
		var el = hold.find('.hold');
		var el2 = hold.find('.hold.pos2');
		var el4 = hold.find('.hold.pos4');

		el.hover(function () {
			$(this).addClass('hover hoverable');
		}, function () {
			$(this).removeClass('hover');
		});

		el2.hover(function () {
			hold.addClass('hover2');
		}, function () {
			hold.removeClass('hover2');
		});

		el4.hover(function () {
			hold.addClass('hover4');
		}, function () {
			hold.removeClass('hover4');
		});
	});
}

function initRoadmapIndex() {
	$('.roadmap-slider').each(function () {
		var hold = $(this);
		var big = hold.find('.roadmap');
		var small = hold.find('.dates');

		big.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
			asNavFor: small,
			dotsClass: 'dots',
			adaptiveHeight: true,
			responsive: [{
				breakpoint: 1030,
				settings: {
					dots: false
				}
			}]
		});
		small.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			asNavFor: big,
			dots: false,
			arrows: false,
			centerMode: true,
			focusOnSelect: true,
			responsive: [{
				breakpoint: 1030,
				settings: {
					variableWidth: true
				}
			}, {
				breakpoint: 750,
				settings: {
					variableWidth: false
				}
			}]
		});

	});
}

function initScroll() {
	var _scroll = function () {
		if ($(window).scrollTop() > 0) {
			$('body').addClass('scroll');
		} else {
			$('body').removeClass('scroll');
		}
	};
	_scroll();
	$(window).bind('scroll', _scroll);
}

/**
 * jQuery tabs min v1.0.0
 * Copyright (c) 2011 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.tabs = function (options) {
	return new Tabs(this.get(0), options);
};

function Tabs(context, options) {
	this.init(context, options);
}
Tabs.prototype = {
	options: {},
	init: function (context, options) {
		this.options = jQuery.extend({
			listOfTabs: 'a.tab',
			active: 'active',
			event: 'click'
		}, options || {});
		this.btn = jQuery(context).find(this.options.listOfTabs);
		this.last = this.btn.index(this.btn.filter('.' + this.options.active));
		if (this.last == -1) this.last = 0;
		this.btn.removeClass(this.options.active).eq(this.last).addClass(this.options.active);
		var _this = this;
		this.btn.each(function (i) {
			jQuery($(this).attr('href')).addClass('hidden-tab');
			if (_this.last == i) jQuery($(this).attr('href')).addClass('visible');
			//else jQuery($(this).attr('href')).hide();
		});
		this.initEvent(this, this.btn);
	},
	initEvent: function ($this, el) {
		el.bind(this.options.event, function () {
			if ($this.last != el.index(jQuery(this))) $this.changeTab(el.index(jQuery(this)));
			return false;
		});
	},
	changeTab: function (ind) {
		jQuery(this.btn.eq(this.last).attr('href')).removeClass('visible');
		jQuery(this.btn.eq(ind).attr('href')).addClass('visible');
		this.btn.eq(this.last).removeClass(this.options.active);
		this.btn.eq(ind).addClass(this.options.active);
		this.last = ind;
	}
}

function detectmob() { 
	if( navigator.userAgent.match(/Android/i)
	|| navigator.userAgent.match(/webOS/i)
	|| navigator.userAgent.match(/iPhone/i)
	|| navigator.userAgent.match(/iPod/i)
	|| navigator.userAgent.match(/iPad/i)
	|| navigator.userAgent.match(/BlackBerry/i)
	|| navigator.userAgent.match(/Windows Phone/i)
	){
	   return true;
	 }
	else {
	   return false;
	 }
}

$(window).on('load resize', function() {
	if ($(window).width() > 1024) {
		$('.maintech-stick').stickySidebar({
			containerSelector: '.maintech-block',
			topSpacing: 160,
			bottomSpacing: 100
		});
	} else {
		$('.maintech-stick').css({
			position: 'static'
		})
	}
	if (!detectmob()) {
	} else {
		$('.inmedia-item').each(function(i, item) {
			if (i >= 4) {
				$(item).addClass('inmedia-item_disable');
			}
		});

		$('.inmedia__more').addClass('inmedia__more_active');

		if (!$('.videomain-block').hasClass('slick-slide')) {
			$('.videomain-block').addClass('slick-slide');
		}

		$('.videomain-slider').slick({
			dots: false,
			centerMode: true,
			arrows: false,
			centerPadding: '5%',
			slidesToShow: 1,
			slidesToScroll: 1
		});

		$('.inmedia__more').on('click', function (e) {
			e.preventDefault();
	
			$('.inmedia-item').each(function(i, item) {
				$(item).removeClass('inmedia-item_disable');
			});
	
			$(this).removeClass('inmedia__more_active');
		});

		$('.mainsectionjs__show, .js-mobiletitle').on('click', function(e) {
			e.preventDefault();

			var _this = $(this).closest('.mainsectionjs');
			_this.toggleClass('mainsectionjs-active');
			_this.find('.mainsectionjs__show').toggleClass('mainsectionjs__show_active');
			_this.find('.mainsectionjs-block').toggleClass('mainsectionjs-block-active');
			_this.find('.quickNews-contact-tablet').toggleClass('quickNews-contact-tablet_active');
			$('.coreteam-block').slick('setPosition');
			$('.videomain-slider').slick('setPosition');
		});
	}

	if ($(window).width() < 900) {
		$('.maintech-stick').css({
			position: 'static'
		})
	}

	$('.toggle-menu').on('click', function() {
		$('.tablet-menu').addClass('tablet-menu_active');
	});

	$('.tablet-menu-top__close').on('click', function(e) {
		e.preventDefault();
		
		$('.tablet-menu').removeClass('tablet-menu_active');
	});

	$('.mobile-menu__toggle').on('click', function(e) {
		e.preventDefault();

		$(this).toggleClass('mobile-menu__toggle_active');
		$(this).toggleClass('mobile-menu-info_active');
	})
});