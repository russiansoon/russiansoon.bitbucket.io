'use strict';

$(document).on('click', 'a[href^="#"]', function (event) {
	event.preventDefault();

	$('#menuToggle input').trigger('click');

	$('html, body').animate({
		scrollTop: $($.attr(this, 'href')).offset().top
	}, 500);
});

$(document).ready(function () {
	initInputs();
	openClose();

	var settings_slider = {
		dots: false,
		slidesToShow: 1,
		centerMode: true,
		centerPadding: '20px'
	};

	slick_on_mobile($('.credentials-block'), settings_slider);
	slick_on_mobile($('.partner-links'), settings_slider);
	slick_on_mobile($('.team-block'), settings_slider);

	function slick_on_mobile(slider, settings) {
		$(window).on('load resize', function () {
			if ($(window).width() > 1160) {
				if (slider.hasClass('slick-initialized')) {
					slider.slick('unslick');
				}
				return;
			}
			if (!slider.hasClass('slick-initialized')) {
				return slider.slick(settings);
			}
		});
	};
});

var initInputs = function initInputs() {
	$('.input input, .input textarea').each(function (index, element) {
		var _el = $(element);
		var _val = _el.val();

		if (_val === '') {
			$(undefined).parent().addClass('placeholder');
		}

		_el.bind('focus', function (el) {
			if (el.target.value == _val) {
				$(el).parent().removeClass('placeholder');
			}
			$(undefined).parent().addClass('input-focus');
		}).bind('blur', function () {
			if (this.value == '') {
				$(this).parent().addClass('placeholder');
			} else {
				$(this).parent().removeClass('placeholder');
			}
			$(this).parent().removeClass('input-focus');
		});
	});
};

function openClose() {
	var $popup = $('.popup');

	$('.js-open').on('click', function (e) {
		e.preventDefault();

		function animateElements(callback) {
			$popup.fadeIn().toggleClass('popup_active');
			callback();
		}

		animateElements(function () {
			$('.popup-decor').addClass('active');
			$('.popup-decor__blue').animate({
				left: '+=200',
				bottom: '+=100',
				transform: 'scale(2.5)'
			}, 1000);
		});

		$('.overlayp').show();

		$('.popup-form').removeClass('popup-hide');
	});

	$('.js-close').on('click', function (e) {
		e.preventDefault();
		$('.popup-decor').removeClass('active');
		$popup.fadeOut().toggleClass('popup_active');
		$(this).closest('.popup-item').addClass('popup-hide');
	});
}

(function () {
	$(".lead-form").submit(function (e) {
		e.preventDefault();

		var _this = $(this);
		var submit = _this.find('#submit');

		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		var email = _this.find('.input__email');

		if (validateEmail(email.val())) {
			submit.prop("disabled", true);
			email.removeClass('input__error');

			var form_data = _this.serializeArray();

			$.ajax({
				type: "POST",
				url: "ajax.php",
				data: form_data,
				success: function success() {
					$('.popup').addClass('popup_active');
					$('.popup-form').addClass('popup-hide');
					$('.popup-ok').removeClass('popup-hide');
					_this.trigger('reset');
					initInputs();

					submit.prop("disabled", false);
				},
				error: function error() {
					submit.prop("disabled", false);
				}
			});
		} else {
			email.addClass('input__error');
		}
	});
})();